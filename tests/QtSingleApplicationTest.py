import sys

from PyQt5 import QtWidgets

import QtSingleApplication


appGuid = 'F3FF80BA-BA05-4277-8063-82A6DB9245A2'
app = QtSingleApplication.QtSingleApplication(appGuid, sys.argv)
if app.isRunning():
    sys.exit(0)

w = QtWidgets.QWidget()
w.show()
app.setActivationWindow(w)
sys.exit(app.exec_())
