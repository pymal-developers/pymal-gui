import glob

from setuptools import setup, find_packages


# Static version
version = "0.1a1"

setup(
    name='pymal-gui',
    packages=find_packages(exclude=['tests*']),
    version=version,
    description='A python gui for the website MyAnimeList (or MAL).',
    author='pymal-developers',
    license="BSD",
    url='https://bitbucket.org/pymal-developers/pymal-gui/',
    keywords=[
        "MyAnimeList", "myanimelist",
        "MAL", "mal",
        "pymal", "pymal-gui",
        "my anime list", "anime list", "anime",
        "Gui", "gui"
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: X11 Applications',
        'Environment :: X11 Applications :: Qt',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Natural Language :: Japanese',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.0',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Database',
        'Topic :: Database :: Front-Ends',
        'Topic :: Multimedia',
        'Topic :: Multimedia :: Graphics',
        'Topic :: Multimedia :: Graphics :: Viewers',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Software Development',
        'Topic :: Software Development :: User Interfaces',
    ],
    install_requires=[
        'pymal>=0.1',
        'PyQt5',
    ],
    data_files=[("bitmaps", glob.glob("icons\\*"))]
)
