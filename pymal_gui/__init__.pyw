import sys
import logging
import os

from qttypes import QtSingleApplication
import MainWindow
import constants
import logging.handlers


def set_logger():
    if not os.path.exists(constants.DATA_DIRECTORY):
        os.makedirs(constants.DATA_DIRECTORY)

    # get root logger
    root_logger = logging.getLogger('')
    # set a format which is simpler for console_log use
    formatter = logging.Formatter(
        fmt=constants.LOG_FORMAT,
        datefmt=constants.DATE_FORMAT
    )

    # define a Handler which writes DEBUG messages or higher to the log
    rotate_log = logging.handlers.RotatingFileHandler(
        constants.LOG_FILE,
        maxBytes=constants.MAX_SIZE_OF_LOG,
        backupCount=constants.BACKUP_COUNT

    )
    rotate_log.setLevel(constants.LOG_LEVEL)
    # tell the handler to use this format
    rotate_log.setFormatter(formatter)
    # add the handler to the root logger
    root_logger.addHandler(rotate_log)

    # define a Handler which writes DEBUG messages or higher to the sys.stderr
    console_log = logging.StreamHandler()
    console_log.setLevel(constants.LOG_LEVEL)
    # tell the handler to use this format
    console_log.setFormatter(formatter)
    # add the handler to the root logger
    root_logger.addHandler(console_log)


def main():
    set_logger()

    if not os.path.exists(constants.TEMP_DIRECTORY):
        os.makedirs(constants.TEMP_DIRECTORY)

    logging.info('Starting application')
    app = QtSingleApplication.QtSingleApplication(constants.APP_GUID, sys.argv)
    if app.isRunning():
        logging.info('Application is already running')
        sys.exit(0)

    window = MainWindow.MainWindow()
    app.setActivationWindow(window)
    sys.exit(app.exec_())


if '__main__' == __name__:
    main()
