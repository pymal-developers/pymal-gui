import constants
from icon_themes.IconTheme import IconTheme
import global_functions


def get_all_themes() -> frozenset:
    has_icon_theme = lambda x: global_functions.has_module_type(x, IconTheme)
    return global_functions.get_all_modules(constants.ICONS_DIRECTORY, has_icon_theme)
