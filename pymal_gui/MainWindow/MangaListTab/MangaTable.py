import threading

from PyQt5 import QtWidgets

from qttypes import QTableWidgetItemUneditable


class MangaTable(QtWidgets.QTableWidget):
    __TABLE_HEADER = ('Name', 'Chapters', 'Volumes', 'Read', 'Score', 'Type')

    def __init__(self, list_to_show, data_widget, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.itemClicked.connect(lambda x: data_widget.change(x.manga))

        self.setColumnCount(len(self.__TABLE_HEADER))
        self.setHorizontalHeaderLabels(self.__TABLE_HEADER)
        self.setRowCount(len(list_to_show))

        threads = list(map(lambda x: threading.Thread(target=x.reload), list_to_show))
        for thread in threads:
            thread.start()

        # TODO: make the thread to call things in the gui (or by event?)
        for thread in threads:
            thread.join()

        for i, manga in enumerate(list_to_show):
            title_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.title)
            title_item.manga = manga
            self.setItem(i, 0, title_item)

            chapters_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(manga.chapters))
            chapters_item.manga = manga
            self.setItem(i, 1, chapters_item)

            volumes_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(manga.volumes))
            volumes_item.manga = manga
            self.setItem(i, 2, volumes_item)

            completed_chapters_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(
                str(manga.my_completed_chapters))
            completed_chapters_item.manga = manga
            self.setItem(i, 3, completed_chapters_item)

            my_score_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(manga.my_score))
            my_score_item.manga = manga
            self.setItem(i, 4, my_score_item)

            type_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.type)
            type_item.manga = manga
            self.setItem(i, 5, type_item)
