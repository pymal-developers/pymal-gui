from PyQt5 import QtWidgets

from MainWindow.MangaListTab import MangaDataWidget, MangaTable


class MangaListTab(QtWidgets.QWidget):
    """
    The manga list tab widget.
    """

    def __init__(self, account, data_widget, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.account = account
        self.data_widget = data_widget

        self.__init_widget()

    def __init_widget(self):
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        self.toolbar = QtWidgets.QToolBar(self)
        main_layout.addWidget(self.toolbar)

        self.__init_toolbar()

        central_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(central_layout)

        self.__data_widget = self.__init_data_widget()
        central_layout.addWidget(self.__data_widget)

        tabs_widget = QtWidgets.QTabWidget(self, movable=True)
        central_layout.addWidget(tabs_widget)

        tabs_widget.addTab(MangaTable.MangaTable(self.account.mangas.reading, self.__data_widget, self), "Reading")
        tabs_widget.addTab(MangaTable.MangaTable(self.account.mangas.on_hold, self.__data_widget, self), "On hold")
        tabs_widget.addTab(MangaTable.MangaTable(self.account.mangas.plan_to_read, self.__data_widget, self), "PTR")
        tabs_widget.addTab(MangaTable.MangaTable(self.account.mangas.completed, self.__data_widget, self), "Completed")
        tabs_widget.addTab(MangaTable.MangaTable(self.account.mangas.dropped, self.__data_widget, self), "Dropped")

    def __init_toolbar(self):
        resetFiltersAction = self.toolbar.addAction('&Reset Filters')

    def __init_data_widget(self):
        return MangaDataWidget.MangaDataWidget(self)
