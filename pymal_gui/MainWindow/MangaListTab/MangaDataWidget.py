import os
import logging

from PyQt5 import QtWidgets, QtGui

import constants
from qttypes import QTableWidgetItemUneditable


class MangaDataWidget(QtWidgets.QWidget):
    WIDTH = 225
    __TABLE_HEADER = ('English', 'Synonyms', 'Japanese', 'Type', 'Chapters', 'Volumes', 'Aired')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFixedWidth(self.WIDTH)

        global_data_layout = QtWidgets.QVBoxLayout()
        self.setLayout(global_data_layout)

        self.__pic = QtWidgets.QLabel("Image", self)
        global_data_layout.addWidget(self.__pic)
        self.__pic.setMinimumHeight(self.WIDTH)
        self.__pic.setMaximumHeight(self.WIDTH * 1.5)

        self.data_table_widget = QtWidgets.QTableWidget(self)
        global_data_layout.addWidget(self.data_table_widget)

        self.data_table_widget.setRowCount(len(self.__TABLE_HEADER))
        self.data_table_widget.setVerticalHeaderLabels(self.__TABLE_HEADER)
        self.data_table_widget.setColumnCount(1)
        self.data_table_widget.horizontalHeader().setVisible(False)

        for i in range(len(self.__TABLE_HEADER)):
            self.data_table_widget.setItem(0, i, QTableWidgetItemUneditable.QTableWidgetItemUneditable(""))

    def change(self, manga):
        manga_path = os.path.join(constants.TEMP_ANIME_DIRECTORY, str(manga.id))
        if not os.path.exists(manga_path):
            os.makedirs(manga_path)

        manga_image_path = os.path.join(manga_path, 'image.png')
        if not os.path.exists(manga_image_path):
            manga.get_image().save(manga_image_path, "PNG")

        manga_qimage = QtGui.QImage(manga_image_path)
        if manga_qimage.isNull():
            logging.warning("Image of anime " + str(manga) + " is null")
            return
        self.__pic.setPixmap(QtGui.QPixmap.fromImage(manga_qimage))

        self.data_table_widget.setItem(0, 0, QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.english))
        self.data_table_widget.setItem(0, 1, QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.synonyms))
        self.data_table_widget.setItem(0, 2, QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.japanese))
        self.data_table_widget.setItem(0, 3, QTableWidgetItemUneditable.QTableWidgetItemUneditable(manga.type))
        self.data_table_widget.setItem(0, 4, QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(manga.chapters)))
        self.data_table_widget.setItem(0, 4, QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(manga.volumes)))
        # self.data_table_widget.setItem(0, 5, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.start_time))
