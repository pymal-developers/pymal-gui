import threading

from PyQt5 import QtWidgets

from qttypes import QTableWidgetItemUneditable


class AnimeTable(QtWidgets.QTableWidget):
    __TABLE_HEADER = ('Name', 'Episodes', 'Watched', 'Score', 'Type')

    def __init__(self, list_to_show, data_widget, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.itemClicked.connect(lambda x: data_widget.change(x.anime))

        self.setColumnCount(len(self.__TABLE_HEADER))
        self.setHorizontalHeaderLabels(self.__TABLE_HEADER)
        self.setRowCount(len(list_to_show))

        threads = list(map(lambda x: threading.Thread(target=x.reload), list_to_show))
        for thread in threads:
            thread.start()

        # TODO: make the thread to call things in the gui (or by event?)
        for thread in threads:
            thread.join()

        for i, anime in enumerate(list_to_show):
            title_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.title)
            title_item.anime = anime
            self.setItem(i, 0, title_item)

            episodes_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(anime.episodes))
            episodes_item.anime = anime
            self.setItem(i, 1, episodes_item)

            completed_episodes_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(
                str(anime.my_completed_episodes))
            completed_episodes_item.anime = anime
            self.setItem(i, 2, completed_episodes_item)

            my_score_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(anime.my_score))
            my_score_item.anime = anime
            self.setItem(i, 3, my_score_item)

            type_item = QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.type)
            type_item.anime = anime
            self.setItem(i, 4, type_item)
