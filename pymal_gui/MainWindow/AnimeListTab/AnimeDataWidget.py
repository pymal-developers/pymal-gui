import os
import logging

from PyQt5 import QtWidgets, QtGui

import constants
from qttypes import QTableWidgetItemUneditable


class AnimeDataWidget(QtWidgets.QWidget):
    WIDTH = 225
    __TABLE_HEADER = ('English', 'Synonyms', 'Japanese', 'Type', 'Episodes', 'Aired', 'Duration')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFixedWidth(self.WIDTH)

        global_data_layout = QtWidgets.QVBoxLayout()
        self.setLayout(global_data_layout)

        self.__pic = QtWidgets.QLabel("Image", self)
        global_data_layout.addWidget(self.__pic)
        self.__pic.setMinimumHeight(self.WIDTH)
        self.__pic.setMaximumHeight(self.WIDTH * 1.5)

        self.data_table_widget = QtWidgets.QTableWidget(self)
        global_data_layout.addWidget(self.data_table_widget)

        self.data_table_widget.setRowCount(len(self.__TABLE_HEADER))
        self.data_table_widget.setVerticalHeaderLabels(self.__TABLE_HEADER)
        self.data_table_widget.setColumnCount(1)
        self.data_table_widget.horizontalHeader().setVisible(False)

        for i in range(len(self.__TABLE_HEADER)):
            self.data_table_widget.setItem(0, i, QTableWidgetItemUneditable.QTableWidgetItemUneditable(""))

    def change(self, anime):
        anime_path = os.path.join(constants.TEMP_ANIME_DIRECTORY, str(anime.id))
        if not os.path.exists(anime_path):
            os.makedirs(anime_path)

        anime_image_path = os.path.join(anime_path, 'image.png')
        if not os.path.exists(anime_image_path):
            anime.get_image().save(anime_image_path, "PNG")

        anime_qimage = QtGui.QImage(anime_image_path)
        if anime_qimage.isNull():
            logging.warning("Image of anime " + str(anime) + " is null")
            return
        self.__pic.setPixmap(QtGui.QPixmap.fromImage(anime_qimage))

        self.data_table_widget.setItem(0, 0, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.english))
        self.data_table_widget.setItem(0, 1, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.synonyms))
        self.data_table_widget.setItem(0, 2, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.japanese))
        self.data_table_widget.setItem(0, 3, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.type))
        self.data_table_widget.setItem(0, 4, QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(anime.episodes)))
        # self.data_table_widget.setItem(0, 5, QTableWidgetItemUneditable.QTableWidgetItemUneditable(anime.start_time))
        self.data_table_widget.setItem(0, 6, QTableWidgetItemUneditable.QTableWidgetItemUneditable(str(anime.duration)))
