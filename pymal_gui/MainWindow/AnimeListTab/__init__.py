from PyQt5 import QtWidgets

from MainWindow.AnimeListTab import AnimeDataWidget, AnimeTable


class AnimeListTab(QtWidgets.QWidget):
    """
    The anime list tab widget.
    """

    def __init__(self, account, data_widget, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.account = account
        self.data_widget = data_widget

        self.__init_widget()

    def __init_widget(self):
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        self.toolbar = QtWidgets.QToolBar(self)
        main_layout.addWidget(self.toolbar)

        self.__init_toolbar()

        central_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(central_layout)

        self.__data_widget = self.__init_data_widget()
        central_layout.addWidget(self.__data_widget)

        tabs_widget = QtWidgets.QTabWidget(self, movable=True)
        central_layout.addWidget(tabs_widget)

        tabs_widget.addTab(AnimeTable.AnimeTable(self.account.animes.watching, self.__data_widget, self), "Watching")
        tabs_widget.addTab(AnimeTable.AnimeTable(self.account.animes.on_hold, self.__data_widget, self), "On hold")
        tabs_widget.addTab(AnimeTable.AnimeTable(self.account.animes.plan_to_watch, self.__data_widget, self), "PTW")
        tabs_widget.addTab(AnimeTable.AnimeTable(self.account.animes.completed, self.__data_widget, self), "Completed")
        tabs_widget.addTab(AnimeTable.AnimeTable(self.account.animes.dropped, self.__data_widget, self), "Dropped")

    def __init_toolbar(self):
        resetFiltersAction = self.toolbar.addAction('&Reset Filters')

    def __init_data_widget(self):
        return AnimeDataWidget.AnimeDataWidget(self)
