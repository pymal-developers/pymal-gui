from PyQt5 import QtWidgets


class AccountTab(QtWidgets.QWidget):
    """
    The account tab widget.
    """

    def __init__(self, account, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.account = account

        self.__init_widget()

    def __init_widget(self):
        central_layout = QtWidgets.QVBoxLayout()
        self.setLayout(central_layout)

        central_layout.addWidget(QtWidgets.QLabel("Account name: " + self.account.username))
        central_layout.addWidget(QtWidgets.QLabel("Account id: " + str(self.account.user_id)))
