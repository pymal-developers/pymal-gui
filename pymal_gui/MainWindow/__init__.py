from PyQt5 import QtGui, QtWidgets, QtCore

import global_functions
from MainWindow import AccountTab, AnimeListTab, MangaListTab
import ConnectWindow
import SystemTrayIcon
import constants
from icon_themes import icon_theme


class MainWindow(QtWidgets.QMainWindow):
    """
    The main application window.
    """
    HEIGHT = 600
    WIDTH = 820

    def __init__(self):
        super().__init__()

        self.account = None

        self.__connect_window = ConnectWindow.ConnectWindow(self.firstShow)

        self.__init_tray()

        self.__connect_window.check_old_data()

    @QtCore.pyqtSlot()
    def firstShow(self):
        self.__init_window()

        self.show()

    @QtCore.pyqtSlot()
    def show(self):
        if self.account is None:
            print("never init, showing connect window")
            self.__connect_window.show()
        else:
            print("init, showing self")
            super().show()

    @QtCore.pyqtSlot()
    def hide(self):
        if self.account is None:
            print("never init, hiding connect window")
            self.__connect_window.hide()
        else:
            print("init, hiding self")
            super().hide()

    @QtCore.pyqtSlot()
    def isHidden(self):
        if self.account is None:
            print("never init, return ishidden of connect window")
            return self.__connect_window.isHidden()
        else:
            print("init, return ishidden of self")
            return super().isHidden()

    def changeEvent(self, event: QtCore.QEvent):
        super().changeEvent(event)
        if QtCore.QEvent.WindowStateChange == event.type():
            if self.isMinimized():
                self.hide()

    def __init_tray(self):
        self.tray = SystemTrayIcon.SystemTrayIcon(QtGui.QIcon(icon_theme.WINDOW_ICON), self)
        self.tray.show()

    def __init_window(self):
        self.account = self.__connect_window.account
        self.setGeometry(0, 0, self.WIDTH, self.HEIGHT)
        self.setMinimumSize(self.WIDTH, self.HEIGHT)

        self.setWindowTitle('pymal - gui - connected as ' + self.account.username)
        self.setWindowIcon(QtGui.QIcon(icon_theme.WINDOW_ICON))

        self.__init_main_layout()
        self.__init_menubar()
        self.__init_statusbar()

        global_functions.center_on_screen(self)

    def __init_menubar(self):
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')

        exitAction = QtWidgets.QAction(QtGui.QIcon(icon_theme.EXIT_ICON), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QtWidgets.qApp.quit)
        fileMenu.addAction(exitAction)

    def __init_statusbar(self):
        self.statusBar().showMessage('Ready')

    def __init_main_layout(self):
        self.__central_widget = QtWidgets.QTabWidget(self, movable=True)
        self.setCentralWidget(self.__central_widget)

        self.__init_account_tab()
        self.__init_anime_list_tab()
        self.__init_manga_list_tab()

    def __init_account_tab(self):
        self.__account_tab = AccountTab.AccountTab(self.account, self.__central_widget)
        self.__central_widget.addTab(self.__account_tab, "Account data")

    def __init_anime_list_tab(self):
        self.__anime_list_tab = AnimeListTab.AnimeListTab(self.account, self.__central_widget)
        self.__central_widget.addTab(self.__anime_list_tab, "Anime list")

    def __init_manga_list_tab(self):
        self.__manga_list_tab = MangaListTab.MangaListTab(self.account, self.__central_widget)
        self.__central_widget.addTab(self.__manga_list_tab, "Manga list")
