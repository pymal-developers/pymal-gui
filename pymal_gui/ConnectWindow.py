import os
import base64
from xml.etree import ElementTree
import logging

from PyQt5 import QtGui, QtWidgets
from pymal import Account

import global_functions
import constants
from icon_themes import icon_theme


class ConnectWindow(QtWidgets.QWidget):
    """
    The connect window - the first window to show!
    """
    HEIGHT = 150
    WIDTH = 150

    def __init__(self, trigger):
        logging.info('Creating ConnectWindow')
        super().__init__()

        self.__trigger = trigger

        self.__init_window()

        self.show()
        logging.info('Finished to create ConnectWindow')

    def __init_window(self):
        self.setGeometry(300, 300, self.WIDTH, self.HEIGHT)
        self.setFixedSize(self.WIDTH, self.HEIGHT)

        self.setWindowTitle('Connect')
        self.setWindowIcon(QtGui.QIcon(icon_theme.WINDOW_ICON))

        global_functions.center_on_screen(self)

        self.__init_main_layout()

    def __init_main_layout(self):
        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)

        self.__init_username_layout(main_layout)
        self.__init_password_layout(main_layout)

        connect_btn = QtWidgets.QPushButton('Connect', self)
        main_layout.addWidget(connect_btn)
        connect_btn.clicked.connect(self.__is_valid)
        connect_btn.setAutoDefault(True)

        self.__error_label = QtWidgets.QLabel("", self)
        main_layout.addWidget(self.__error_label)
        self.__error_label.setStyleSheet("QLabel { color : red; }")
        self.__username_line_edit.textChanged.connect(self.__error_label.clear)
        self.__password_line_edit.textChanged.connect(self.__error_label.clear)

    def check_old_data(self):
        logging.info('Loading old data')
        if not os.path.exists(constants.DATA_DIRECTORY):
            os.makedirs(constants.DATA_DIRECTORY)
        if not os.path.exists(constants.USER_DATA):
            return

        tree = ElementTree.parse(constants.USER_DATA)
        root = tree.getroot()
        username_element, password_element = root.getchildren()
        self.__username_line_edit.setText(username_element.text)
        password = base64.decodebytes(password_element.text.encode()).decode()
        self.__password_line_edit.setText(password)

        self.__is_valid()

    def __is_valid(self):
        self.__error_label.setText("Authenticating...")

        username = self.__username_line_edit.text()
        logging.info('Authenticating for ' + username)
        password = self.__password_line_edit.text()
        self.account = Account.Account(username)

        if self.account.change_password(password):
            logging.info('Succeed to authenticate')
            self.__write_user_data(username, password)
            self.__trigger()
            self.hide()
        else:
            logging.info('Failed to authenticate')
            self.__error_label.setText("Failed to authenticate...")

    def __write_user_data(self, username: str, password: str):
        tree = ElementTree.ElementTree()

        root = ElementTree.Element('user_data')
        tree._setroot(root)

        username_element = ElementTree.Element('username')
        username_element.text = username
        root.append(username_element)

        password_element = ElementTree.Element('password')
        password_element.text = base64.encodebytes(password.encode()).decode()
        root.append(password_element)

        if not os.path.exists(constants.DATA_DIRECTORY):
            os.makedirs(constants.DATA_DIRECTORY)
        tree.write(constants.USER_DATA, encoding="utf-8", xml_declaration=True, method="xml")

    def __init_username_layout(self, main_layout):
        username_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(username_layout)
        username_layout.addWidget(QtWidgets.QLabel("Username:", self))
        self.__username_line_edit = QtWidgets.QLineEdit("", self)
        username_layout.addWidget(self.__username_line_edit)

    def __init_password_layout(self, main_layout):
        password_layout = QtWidgets.QHBoxLayout()
        main_layout.addLayout(password_layout)
        password_layout.addWidget(QtWidgets.QLabel("Password:", self))
        self.__password_line_edit = QtWidgets.QLineEdit("", self)
        password_layout.addWidget(self.__password_line_edit)
        self.__password_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
