from os import path
import logging

import icon_themes

# Directories
DIRECTORY_SUFFIX = path.join('pymal', 'gui')

DATA_DIRECTORY = path.join(path.expandvars('%ALLUSERSPROFILE%'), DIRECTORY_SUFFIX)
TEMP_DIRECTORY = path.join(path.expandvars('%LocalAppData%'), DIRECTORY_SUFFIX)
ICONS_DIRECTORY = path.dirname(icon_themes.__file__)

USER_DATA = path.join(DATA_DIRECTORY, "user_data.xml")

TEMP_ANIME_DIRECTORY = path.join(TEMP_DIRECTORY, 'anime')
TEMP_MANGE_DIRECTORY = path.join(TEMP_DIRECTORY, 'manga')

APP_GUID = 'F3FF80BA-BA05-4277-8063-82A6DB9245A2'

# Logging config
LOG_FILE = path.join(DATA_DIRECTORY, "pymal-gui.log")
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = '[%(asctime)s] %(levelname)s: %(message)s'
DATE_FORMAT = '%Y-%d-%m %I:%M:%S'
MAX_SIZE_OF_LOG = 1000 * 1000
BACKUP_COUNT = 3

