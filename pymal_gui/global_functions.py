import sys
import os

from PyQt5 import QtWidgets


def center_on_screen(window: QtWidgets.QWidget):
    """
    Centering a window on the screen.
    :param window: a window so center on the screen.
    :type window: QtGui.QWidget
    """
    resolution = QtWidgets.QDesktopWidget().screenGeometry()
    window.move((resolution.width() / 2) - (window.frameSize().width() / 2),
                (resolution.height() / 2) - (window.frameSize().height() / 2))


def is_module(directory: str) -> bool:
    """
    Checking if a directory is a module.
    :param directory:
    :type directory:
    :return:
    :rtype:
    """
    return os.path.exists(os.path.join(directory, '__init__.py'))


def has_module_type(module, wanted_type) -> bool:
    """
    Return true if has an IconTheme object.
    :param module:
    :type module:
    :return:
    :rtype:
    """
    public_list = filter(lambda x: not x.startswith('_'), dir(module))
    public_attributes_list = map(lambda x: getattr(module, x), public_list)
    public_types_list = filter(lambda x: isinstance(x, type), public_attributes_list)
    icon_themes_list = map(lambda x: issubclass(x, wanted_type) or x.__class__ == wanted_type.__class__, public_types_list)
    return any(icon_themes_list)


def get_all_modules(directory: str, inner_checker) -> frozenset:
    """
    Gets a directory and return all the modules in it.
    :param directory:
    :type directory:
    :return:
    :rtype:
    """
    directory_list = os.listdir(directory)
    full_directory_list = map(lambda x: os.path.join(directory, x), directory_list)
    module_list = filter(is_module, full_directory_list)
    sys.path.append(directory)
    try:
        imported_modules_list = list(map(__import__, map(os.path.basename, module_list)))
    finally:
        sys.path.remove(directory)
    wanted_modules = filter(inner_checker, imported_modules_list)
    return frozenset(wanted_modules)
