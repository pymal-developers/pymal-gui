from PyQt5 import QtWidgets, QtCore


class QTableWidgetItemUneditable(QtWidgets.QTableWidgetItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setFlags(QtCore.Qt.ItemIsEnabled)