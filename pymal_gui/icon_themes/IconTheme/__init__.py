__author__ = 'user'

from os import path

ICONS_DIRECTORY = path.dirname(__file__)


class IconTheme(object):
    """
    Basic class for all icon themes.
    If you don't change all the icons they will be taken from here by inheritance.
    """
    WINDOW_ICON = path.join(ICONS_DIRECTORY, "main_window.png")
    EXIT_ICON = path.join(ICONS_DIRECTORY, "exit.png")
    PLUS_ICON = path.join(ICONS_DIRECTORY, "plus.png")
    MINUS_ICON = path.join(ICONS_DIRECTORY, "minus.png")
