from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QSystemTrayIcon

import constants
from icon_themes import icon_theme


class SystemTrayIcon(QSystemTrayIcon):
    """
    A class for system tray icon - easier to handle it here.
    """

    def __init__(self, icon: QtGui.QIcon, parent: QtCore.QObject=0):
        super().__init__(icon, parent)
        self.__parent = parent

        self.__init_context_menu()

        self.activated.connect(self.iconActivated)

    def __init_context_menu(self):
        menu = QtWidgets.QMenu(self.__parent)
        self.setContextMenu(menu)

        hideShowAction = QtWidgets.QAction('&Show\Hide', self)
        hideShowAction.setStatusTip('Show\Hide')
        hideShowAction.triggered.connect(self.showHideParent)
        menu.addAction(hideShowAction)

        menu.addSeparator()

        exitAction = QtWidgets.QAction(QtGui.QIcon(icon_theme.EXIT_ICON), '&Exit', self)
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QtWidgets.qApp.quit)
        menu.addAction(exitAction)

    @QtCore.pyqtSlot()
    def showHideParent(self):
        if self.__parent.isHidden():
            self.__parent.show()
        else:
            self.__parent.hide()

    @QtCore.pyqtSlot(QSystemTrayIcon.ActivationReason)
    def iconActivated(self, reason: QSystemTrayIcon.ActivationReason):
        if QSystemTrayIcon.Trigger == reason or QSystemTrayIcon.DoubleClick == reason:
            self.showHideParent()
